import axios from "axios"
import { BASE_URL } from "../../api"
import { actionTypes } from "./actionType"

// create function to get products 
export const getAllProducts = () => {
    return(dispatch) =>{
        axios(`${BASE_URL}products`)
        .then(res=>dispatch({
            type: actionTypes.GET_PRODUCTS,
            payload: res.data
        }))
        .catch(er=>{
            console.log('issue in fetch product')
        })
    }
}

// create function to search products
export const searchProducts = (title) => {
    return(dispatch) =>{
        axios(`${BASE_URL}products?title${title}`)
        .then(res=>dispatch({
            type: actionTypes.SEARCH_PRODUCT,
            payload: res.data
        }))
        .catch(er=>{
            console.log('issue in search product',er)
        })
    }
}