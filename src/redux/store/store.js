import { lazy } from "react";
import { applyMiddleware, compose, legacy_createStore } from "redux";
import { thunk } from "redux-thunk";
import { rootReducer } from "../reducers/rootReducer";

// store run root reducer
const middleWare = [thunk]
export const store = legacy_createStore(
    rootReducer, compose(applyMiddleware(...middleWare))
)