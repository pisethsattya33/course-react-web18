import { actionTypes } from "../action/actionType"

//  reducer is function which check on action and return state
const initialState = {
    products:[],
    isLoading: true
}

export const productReducer = (state = initialState,action) => {
    let {GET_PRODUCTS,SEARCH_PRODUCT} = actionTypes
    let {type,payload} = action

    switch(type){
        case GET_PRODUCTS:
            return{...state, products: payload, isLoading: false}

        case SEARCH_PRODUCT:
            return{...state,products: payload, isLoading:false}
        default:
            return state
    }
}