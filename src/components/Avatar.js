// named export
export function Avatar({name, image, date, isActive}){
        return(
            <div class="flex items-center gap-4">
                <img class="w-10 h-10 rounded-full" src={image} alt="" />
                <div class="font-medium dark:text-white">
                    <div>{isActive ? name + "✔️" : name}</div>
                    <div class="text-sm text-gray-500 dark:text-gray-400">{date}</div>
                </div>
            </div>
        )
    
}


// default export
export default function Profile(){
    const people = [
        {
            name: "Reksmey",
            image: "https://flowbite.com/docs/images/people/profile-picture-5.jpg",
            info: "Joined on 2024",
            isActive: true

        },
        {
            name: "Reksa",
            image: "https://flowbite.com/docs/images/people/profile-picture-5.jpg",
            info: "Joined on 2024",
            isActive: false
        },
        {
            name: "Rotha",
            image: "https://flowbite.com/docs/images/people/profile-picture-5.jpg",
            info: "Joined on 2024",
            isActive: true
        },
        {
            name: "Lisa",
            image: "https://flowbite.com/docs/images/people/profile-picture-5.jpg",
            info: "Joined on 2024",
            isActive: false 
        }
    ]
    return(
        <>
            <h2>List of Instructors</h2>
            {/* rendering as list */}
            {
                people.map(person => (
                    <>
                        <h3>{person.name}</h3>
                        <Avatar 
                            name={person.name}
                            image={person.image}
                            date={person.info}
                            isActive={person.isActive}
                        />
                    </>
                ))
            }
  
        </>
    )
}

    
    

    
    
