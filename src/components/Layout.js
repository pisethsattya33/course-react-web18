import { Outlet } from "react-router-dom";
import Footer from "./Footer";
import Navbar, { AdminNavbar } from "./Navbar";
import { useEffect } from "react";
import Aos from "aos";
import 'aos/dist/aos.css';

export default function MainLayout() {
  useEffect(()=>{
    Aos.init()
  },[])
  return (
    <>
        <Navbar />
            <Outlet />
        <Footer />
    </>
  )
}

export function AdminLayout(){
  return(
    <>
        {/* will put admin navbar */}
        <AdminNavbar />
        <Outlet />
    </>
  )
}
