import { useEffect, useState } from "react";
import DataTable from "react-data-table-component";
import { Modals } from "./Modals";
import { Navigate, useNavigate } from "react-router-dom";
import { Button, TextInput } from "flowbite-react";
import { useDispatch, useSelector } from "react-redux";
import { searchProducts } from "../redux/action/productActions";

export default function ProductDataTable(){

    const [query, setQuery] = useState("")

    const navigate = useNavigate()
    const dispatch = useDispatch()

    // received global state
    const {products} = useSelector(state=>state.productR)
    const {isLoggedIn} = useSelector(state=>state.authR)

    useEffect(() => {
        dispatch(searchProducts(query))
        console.log("is login",isLoggedIn)
    }, [query])

    const columns = [
        {
            name: 'Title',
            selector: row => row.title,
            sortable: true
        },
        {
            name: 'Category',
            selector: row => row.category.name,
            sortable: true
        },
        {
            name: 'Price',
            selector: row => `${row.price} $`,
            sortable: true
        },
        {
            name: 'Thumbnail',
            selector: row => <img
                className="w-[100px] m-2"
                src={row.images[0]} />,
        },
        {
            name: "Action",
            selector: row => <div className="grid grid-cols-3 gap-2">
                <Modals data={row} />
                <Button
                onClick={() => navigate("/product/insert")}
                color="blue">Edit</Button>
                <Button 
                
                color="failure">Delete</Button>
            </div>
        }
    ];
    
    return isLoggedIn ? (
        <DataTable 
            data={products}
            columns={columns}
            pagination
            subHeader
            subHeaderComponent={
              <div className="w-1/2">
                <TextInput
                   onChange={(e) =>{
                    setQuery(e.target.value)
                   }}
                   type="text"
                   placeholder="Searching..."
                   required
                   shadow
                />
              </div>
            }
        />
    ) : (
        <Navigate to={"/unauthorized"}/>
    )
}