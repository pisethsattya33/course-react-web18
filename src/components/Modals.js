'use client';

import { Button, Modal } from 'flowbite-react';
import { useState } from 'react';

export function Modals({data}) {
  const [openModal, setOpenModal] = useState(false);

  console.log(data)

  return (
    <>
      <Button onClick={() => setOpenModal(true)}>View</Button>
      <Modal show={openModal} onClose={() => setOpenModal(false)}>
        <Modal.Header>{data.title}</Modal.Header>
        <Modal.Body>
          <div className="space-y-6">
            <p className="text-base leading-relaxed text-gray-500 dark:text-gray-400">
              {data.description}
            </p>
            <p className="text-base leading-relaxed text-gray-500 dark:text-gray-400">
              {data.price}
            </p>
            <div className='grid grid-cols-3 gap-2'>
                {
                    data.images.map((image) => <img 
                        src={image} alt={data.title} />)
                }
            </div>
            
          </div>
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={() => setOpenModal(false)}>Ok</Button>
          <Button color="gray" onClick={() => setOpenModal(false)}>
            Close
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
}
