// Import Swiper React components
import { Swiper, SwiperSlide } from 'swiper/react';
import {Autoplay, Navigation, Pagination} from 'swiper/modules';

// Import Swiper styles
import 'swiper/css';
import 'swiper/css/navigation';
import 'swiper/css/pagination';
import 'swiper/css/autoplay';
import { Card } from './Card';


export function MySlider({data,perView}){
    
    return(
        <Swiper
        spaceBetween={20}
        slidesPerView={perView}
        autoplay = {{
            delay: 3000,
            disableOnInteraction: false
        }}
         // install Swiper modules
        modules={[Autoplay,Navigation, Pagination]}
        onSlideChange={() => console.log('slide change')}
        onSwiper={(swiper) => console.log(swiper)}
        >
        {
            data.map(d=><SwiperSlide>
                <Card product={d}/>
            </SwiperSlide>)
        }
        <SwiperSlide>Slide 1</SwiperSlide>
        </Swiper>
    )
}