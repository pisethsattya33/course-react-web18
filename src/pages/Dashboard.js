import { initFlowbite } from "flowbite"
import { useEffect } from "react"
import ProductDataTable from "../components/ProductDataTable"

export default function Dashboard(){
    useEffect(() => {
        initFlowbite()
    },[])
    return(
        <>
            {/* <!-- Left Sidebar --> */}
            

            <main class="p-4 md:ml-64 h-auto pt-20">
                {/* call data table */}
                <ProductDataTable />
            </main>
        </>
    )
}