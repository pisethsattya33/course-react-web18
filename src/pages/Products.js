import React, { useEffect, useState } from 'react'
import { fetchProducts } from '../services/productActions'
import Loading from '../components/Loading'
import { Card } from '../components/Card'
import { SlideShow } from '../components/SlideShow'
import FormProduct from '../components/FormProduct'

export default function Products() {
    const [isLoading, setIsLoading] = useState(true)
    const [products, setProducts] = useState([
        {
          title: "Vision Pro",
          images: ["https://eduport.webestica.com/assets/images/courses/4by3/04.jpg"],
          description: "Hehe",
          category: {
            name: "Electronics"
          }
        }
      ])
    
    useEffect(() => {
        fetchProducts(12)
        .then(response => {
            setProducts(response)
            setIsLoading(false)
        })
    }, [isLoading, products])
    
     
  return (
    <main className='mt-20 container mx-auto'>
        <SlideShow />
        <section className='grid grid-cols-1 md:grid-cols-4 gap-4'>    
            {
            isLoading ? <Loading /> : products.map(pro => <Card product={pro}/>)
            }
        </section>
    </main>
  )
}
