import logo from './logo.svg';
import './App.css';
import Profile, { Avatar } from './components/Avatar';
import { useEffect, useState } from 'react';
import { fetchProducts } from './services/productActions';
import { Card } from './components/Card';
import Loading from './components/Loading';
import Navbar from './components/Navbar';
import Footer from './components/Footer';
import { SlideShow } from './components/SlideShow';
import { initFlowbite } from 'flowbite';
import { useDispatch, useSelector } from 'react-redux';
import { getAllProducts } from './redux/action/productActions';
import { MySlider } from './components/MySlider';


function App() {
  const [count, setCount] = useState(0)
  const [index, setIndex] = useState(0)


 // requesting action
const dispatch = useDispatch();

// received global state
let { products,isLoading} = useSelector((state) => state.productR);


  // making a call to function ( api request )
  useEffect(() => {
    initFlowbite()
    dispatch(getAllProducts());
  }, [])

  function handleClick(){
    // increate by 1 of count variable
    setCount(count + 1)
  }
  const handleClick1 = () => {
    setCount(count + 1)
  }
  function handleNextClick(){
    setIndex(index + 1)
    if(index == products.length - 1){
      setIndex(0)
    }
  }

  return (
   <>
      <main className='container mx-auto'>
        {/* <SlideShow /> */}
      <MySlider data={products} perView={5}/>

      {/* <section className='text-center'> */}
      {/* <button
        onClick={() => handleClick1()} 
        type="button" class="text-white bg-gradient-to-br from-purple-600 to-blue-500 hover:bg-gradient-to-bl focus:ring-4 focus:outline-none focus:ring-blue-300 dark:focus:ring-blue-800 font-medium rounded-lg text-sm px-5 py-2.5 text-center me-2 mb-2">
        Your clicked {count} times
      </button>
      <button
         onClick={handleNextClick}
        type="button" class="text-white bg-gradient-to-br from-purple-600 to-blue-500 hover:bg-gradient-to-bl focus:ring-4 focus:outline-none focus:ring-blue-300 dark:focus:ring-blue-800 font-medium rounded-lg text-sm px-5 py-2.5 text-center me-2 mb-2">
        Next
      </button>
      </section>
      <section className='container mx-auto grid grid-cols-1 md:grid-cols-4 gap-4'>    
        <Card product={products[index]}/>
      </section> */}
    {/* <Profile /> */}
      <section className='container mx-auto grid grid-cols-1 md:grid-cols-4 gap-4'>    
        {
          isLoading ? <Loading /> : products.map(pro => <Card product={pro}/>)
        }
      </section>
   </main>
   </>
  );
}

export default App;
