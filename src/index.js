import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import { RouterProvider, createBrowserRouter } from 'react-router-dom';
import About from './pages/About';
import Contact from './pages/Contact';
import Login from './pages/Login';
import MainLayout, { AdminLayout } from './components/Layout';
import NotFound from './components/NotFound';
import Products from './pages/Products';
import DetailProduct from './pages/DetailProduct';
import InsertProduct from './pages/InsertProduct';
import Dashboard from './pages/Dashboard';
import FormProduct from './components/FormProduct';
import ProductFormValidation from './components/FormikProduct';
import { Provider } from 'react-redux';
import { store } from './redux/store/store';
import { Unauthorized } from './pages/Unauthorized';

const router = createBrowserRouter([
  {
    path: "/",
    element: <MainLayout />,
    errorElement: <NotFound />,
    children: [
      {
        path: "/",
        element: <App />
      },
      {
        path: "/products",
        element: <Products />
      },
      {
        path: "/products/:id",
        element: <DetailProduct />
      },
      {
        path: "/about",
        element: <About />
      },
      {
        path: "/contact",
        element: <Contact />
      },
      {
        path: "/create",
        element: <InsertProduct />
      }
    ]
  },
  {
    path: "/login",
    element: <Login />
  },
  {
    path: "/logout",
    element: <Login />
  },
  {
    path: "/unauthorized",
    element: <Unauthorized/>
  },
  {
    path: "/",
    element: <AdminLayout />,
    children: [
      {
        path: '/dashboard',
        element: <Dashboard />
      },
      {
        path: '/admin/insert',
        element: <FormProduct />
      },
      {
        path: '/product/insert',
        element: <ProductFormValidation />
      },

    ]
  }
])

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
  <Provider store={store}>
  <RouterProvider router={router} />
  </Provider>
  </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
